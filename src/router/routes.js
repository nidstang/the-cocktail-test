import Regimes from '@/components/Regimes'
import Improvements from '@/components/Improvements'

export default [
  {
    name: 'regimes',
    path: '/',
    component: Regimes
  },
  {
    name: 'improvements',
    path: '/improvements',
    component: Improvements
  }
]
