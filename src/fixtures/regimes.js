export default [
  {
    name: 'Solo Alojamiento',
    list: [
      'Sin régimen',
      'Botella de agua de bienvenida'
    ]
  },
  {
    name: 'Alojamiento y Desayuno',
    list: [
      'Desayuno buffet completo'
    ]
  },
  {
    name: 'Media Pensión',
    list: [
      'Desayuno buffet completo',
      'Cena buffet',
      'No incluye'
    ]
  },
  {
    name: 'Todo Incluido',
    list: [
      'Dispondrás de comida y bebida todo el día durante toda tu estancia',
      'Restaurante buffet',
      'Snack bar',
      'Servicio de bares (marcas nacionales)'
    ]
  },
  {
    name: 'Unlimited Services',
    list: [
      'Restaurante buffet con bebidas Premium',
      'Restaurante a la cara (una cena por estancia)',
      'Snack bar con bebidas Premium',
      'Minibar incluido',
      'Acceso a SPA y 45 minutos de masaje por persona',
      'Actividades deportivas (acuaticas sin motor)',
      'Caja fuerte',
      'Parking gratuito',
      'Lavendería'
    ]
  }
]
