# Instalacción y puesta en marcha
## Instalando
El proyecto hace uso de npm como gestor de dependencias. Por tanto se necesita tenerlo instalado primero.

Haciendo uso de npm, en la raiz de proyecto ejecutar:

``` npm i ```

## Lanzando el servidor de desarrollo

Para lanzar el dev server, ejecutar:

```
npm run dev
```

## Generando versión para producción
Para generar los ficheros de producción, ejecutar:

```
npm run prod
```