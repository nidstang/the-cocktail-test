export const range = n => {
  return [...Array(n).keys()]
}
