import Vue from 'vue'
import VueRouter from 'vue-router'

// routes
import routes from '@/router/routes'
import App from './App'

// assets
import './assets/sass/main.scss'

Vue.use(VueRouter)
const router = new VueRouter({ routes })

const app = new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
