var merge = require('webpack-merge')
var baseConfig = require('./webpack.base')

module.exports = merge(baseConfig, {
  mode: 'development',
  devServer: {
    historyApiFallback: true,
    noInfo: false,
    overlay: true,
    host: process.env.APP_HOST || 'localhost',
    port: process.env.APP_PORT || 8080,
    disableHostCheck: true,
  },

  performance: {
    hints: false
  },

  devtool: '#eval-source-map'
})
